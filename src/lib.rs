mod arith;
mod cert;
mod curve;
mod primes;

use rug::{integer::SmallInteger, rand::RandState, Complete, Integer};

pub use cert::PrimalityProof;

use arith::Field;
use cert::{disc_gcd, is_bounded, is_small_prime, PrimalityCert};
use curve::{Curve, Point};

pub fn ecpp(n: Integer) -> Option<PrimalityProof> {
    let mut rand = RandState::new();
    ecpp_with(n, &mut rand)
}

fn ecpp_with(n: Integer, rand: &mut RandState) -> Option<PrimalityProof> {
    if let Ok(n) = u16::try_from(&n) { // check nonnegative integer below 2^16
        return is_small_prime(n).then_some(PrimalityProof::new(n));
    }

    if n < 0 || !n.get_bit(0) { // negative or even => composite
        return None;
    }

    let limit = n.clone();
    let field = Field::new(n);
    loop { // generate a (new) random elliptic curve
        let a = Integer::from(limit.random_below_ref(rand));
        let x0 = Integer::from(limit.random_below_ref(rand));
        let y0 = Integer::from(limit.random_below_ref(rand));
        let b = field.sub(
            field.mul(&y0, &y0),
            field.add(field.mul(field.mul(&x0, &x0), &x0), field.mul(&a, &x0)),
        );
        let d = disc_gcd(&a, &b, field.modulus());
        if d == *field.modulus() {
            continue; // bad reduction => try again
        } else if d > 1 {
            return None; // found a factor => composite
        }
        let curve = Curve::new(field.clone(), a, b);
        let m = curve.count().ok()?;

        let t = (&m - curve.modulus()).complete() - 1u32;
        if t.square() > (4u32 * curve.modulus()).complete() {
            return None; // order outside Hasse interval => composite
        }

        let mut q = m;
        let mut c = Integer::from(1);
        for p in [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37] {
            let e = q.remove_factor_mut(&SmallInteger::from(p));
            c *= Integer::u_pow_u(p, e).complete();
        }

        if c == 1 || !is_bounded(&q, curve.modulus()) || !miller_rabin(&q, rand) {
            continue; // no probably prime factor => try again
        }

        let point0 = Point::affine(x0, y0);
        let point1 = curve.mul_scalar(&c, &point0).ok()?;
        if point1.is_zero() {
            continue; // order of point too small => try again
        };

        if !curve.mul_scalar(&q, &point1).ok()?.is_zero() {
            return None; // order of point does not divide order of group => composite
        }

        if let Some(mut proof) = ecpp_with(q.clone(), rand) { // q is prime
            let (p, a, _) = curve.into_inner();
            let (x, y) = point1.get().unwrap();
            let b = y.square_ref() - (x.square_ref() + &a).complete() * &x;
            proof.push(PrimalityCert { p, a, b, x, y, q });
            return Some(proof); // p is prime => return proof
        } // otherwise q is not prime => try again
    }
}

fn miller_rabin(n: &Integer, rand: &mut RandState) -> bool {
    if *n == 2 {
        return true;
    }

    if *n <= 1 || n.is_even() {
        return false;
    }

    let n_minus_one = (n - 1u32).complete();
    let limit = n_minus_one.clone();

    let a = Integer::from(limit.random_below_ref(rand)) + 1u32;
    let s = n.find_one(0).unwrap();
    let t = (&n_minus_one >> s).complete();
    let mut b = a.pow_mod(&t, n).unwrap();

    if b == 1 || b == n_minus_one {
        return true;
    }

    for _ in 1..s {
        b = b.square() % n;
        if b == n_minus_one {
            return true;
        }
    }

    false
}

#[cfg(test)]
mod tests {
    use rayon::prelude::*;

    use super::*;
    use crate::primes::Sieve;

    #[test]
    fn test_ecpp() {
        let mut primes = Sieve::new();
        for block in 0..1 << 8 {
            let start = block * 4096;
            let end = start + 4096;
            while primes.len() < end {
                primes.grow();
            }
            (start..end).into_par_iter().for_each(|i| {
                assert_eq!(ecpp(i.into()).is_some(), primes.get(i));
            });
        }
    }
}
