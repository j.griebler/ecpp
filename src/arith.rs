use std::{
    array,
    cmp::Ordering,
    mem,
    ops::{Add, Mul, Shl, Sub},
};

use rug::{
    integer::{Order, SmallInteger},
    Complete, Integer,
};

pub(crate) fn crt_update(
    acc: Integer,
    prod: Integer,
    rem: u64,
    modulus: u64,
) -> (Integer, Integer) {
    let big_rem = SmallInteger::from(modulus);
    let new_prod = (modulus * &prod).complete();
    let (mod_inv, prod_inv) = inv_mod_recip(&big_rem, &prod);
    let new_acc = (&prod * prod_inv * rem + modulus * mod_inv * acc) % &new_prod;
    (new_acc, new_prod)
}

fn inv_mod_recip(a: &Integer, b: &Integer) -> (Integer, Integer) {
    let (gcd, mut i, mut j) = a.extended_gcd_ref(b).complete();
    assert_eq!(i32::try_from(gcd), Ok(1), "not invertible");
    if i < 0 {
        i += b;
    }
    if j < 0 {
        j += a;
    }
    (i, j)
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct Field { // Z/pZ, presumed to be a field
    modulus: Integer,
}

impl Field {
    pub(crate) fn new(modulus: Integer) -> Self {
        Self { modulus }
    }

    pub(crate) fn into_inner(self) -> Integer {
        self.modulus
    }

    pub(crate) fn modulus(&self) -> &Integer {
        &self.modulus
    }

    pub(crate) fn add<L, R>(&self, a: L, b: R) -> Integer
    where
        L: Add<R>,
        L::Output: Into<Integer>,
    {
        let mut out = (a + b).into();
        if out >= self.modulus {
            out -= &self.modulus;
        }
        out
    }

    pub(crate) fn sub<L, R>(&self, a: L, b: R) -> Integer
    where
        L: Sub<R>,
        L::Output: Into<Integer>,
    {
        let mut out = (a - b).into();
        if out < 0 {
            out += &self.modulus;
        }
        out
    }

    pub(crate) fn neg<T>(&self, a: T) -> Integer
    where
        Integer: Sub<T, Output = Integer>,
    {
        self.sub(Integer::ZERO, a)
    }

    pub(crate) fn mul<L, R>(&self, a: L, b: R) -> Integer
    where
        L: Mul<R>,
        L::Output: Into<Integer>,
    {
        let mut out = (a * b).into();
        if out >= self.modulus {
            out %= &self.modulus;
        }
        out
    }

    pub(crate) fn div<L>(&self, a: L, b: &Integer) -> Result<Integer, CompositeError>
    where
        L: Mul<Integer, Output = Integer>,
    {
        Ok(self.mul(a, self.inv(b)?))
    }

    fn inv(&self, a: &Integer) -> Result<Integer, CompositeError> {
        a.invert_ref(&self.modulus)
            .map(Integer::from)
            .ok_or(CompositeError)
    }

    pub(crate) fn sub_poly(&self, f: &Poly, g: &Poly) -> Poly {
        Poly::by_coeffs([f, g], |[a, b]| self.sub(a, b))
    }

    pub(crate) fn mul_scalar_poly<'a, T>(&self, c: T, f: &'a Poly) -> Poly
    where
        T: Mul<&'a Integer> + Copy,
        T::Output: Into<Integer>,
    {
        Poly::by_coeffs([f], move |[a]| self.mul(c, a))
    }

    pub(crate) fn mul_scalar_add_poly<'a, T>(&self, a: T, f: &'a Poly, g: &'a Poly) -> Poly
    where
        T: Mul<&'a Integer> + Copy,
        T::Output: Into<Integer>,
    {
        Poly::by_coeffs([f, g], |[b, c]| self.add(self.mul(a, b), c))
    }

    pub(crate) fn mul_scalar_sub_poly<'a, T>(&self, a: T, f: &'a Poly, g: &'a Poly) -> Poly
    where
        T: Mul<&'a Integer> + Copy,
        T::Output: Into<Integer>,
    {
        Poly::by_coeffs([f, g], |[b, c]| self.sub(self.mul(a, b), c))
    }

    fn div_scalar_poly(&self, a: &Integer, f: &Poly) -> Result<Poly, CompositeError> {
        Ok(self.mul_scalar_poly(&self.inv(a)?, f))
    }

    fn mul_poly_iter<'a, I1, I2>(
        &'a self,
        iter1: I1,
        iter2: I2,
    ) -> impl DoubleEndedIterator<Item = Integer> + ExactSizeIterator + 'a
    where
        I1: IntoIterator<Item = &'a Integer>,
        I2: IntoIterator<Item = &'a Integer>,
        I1::IntoIter: ExactSizeIterator,
        I2::IntoIter: ExactSizeIterator,
    {
        fn kronecker_subst<'a, I>(iter: I, width: usize) -> Integer
        where
            I: ExactSizeIterator<Item = &'a Integer>,
        {
            let mut int = vec![0u64; iter.len() * width];
            for (i, a) in iter.enumerate() {
                for (j, b) in a.as_ref().iter().copied().enumerate() {
                    int[i * width + j] = b;
                }
            }

            Integer::from_digits(&int, Order::Lsf)
        }

        let iter1 = iter1.into_iter();
        let iter2 = iter2.into_iter();
        let l1 = iter1.len();
        let l2 = iter2.len();
        let m = 2 * self.modulus.significant_bits() as usize
            + (usize::BITS - (Ord::min(l1, l2) - 1).leading_zeros() + 1) as usize;
        let m = (m + 63) / 64;
        let f_num = kronecker_subst(iter1, m);
        let g_num = kronecker_subst(iter2, m);
        let h_num = f_num * g_num;
        let n_chunks = (h_num.as_ref().len() + m - 1) / m;
        let modulus = &self.modulus;
        (0..n_chunks).map(move |i| {
            let digits = h_num.as_ref();
            let start = i * m;
            let end = (start + m).min(digits.len());
            Integer::from_digits(&digits[start..end], Order::Lsf) % modulus
        })
    }

    pub(crate) fn mul_poly(&self, f: &Poly, g: &Poly) -> Poly {
        if f.is_zero() || g.is_zero() {
            return Poly::zero();
        }
        self.mul_poly_iter(&f.coeffs, &g.coeffs).collect()
    }

    pub(crate) fn make_monic(&self, f: &Poly) -> Result<(Integer, Poly), CompositeError> {
        let c = self.inv(f.leading())?;
        let f = self.mul_scalar_poly(&c, f);

        Ok((c, f))
    }

    fn approx_poly_div(&self, div: &Poly, deg: usize) -> Result<Poly, CompositeError> {
        assert!(!div.is_zero());
        assert!(div.is_monic());

        let m = deg.saturating_sub(div.deg());
        let k = usize::BITS - m.leading_zeros();
        let mut inv = Poly::one();
        for i in 1..=k {
            let prod = self
                .mul_poly_iter(div.coeffs.iter().rev(), &self.mul_poly(&inv, &inv).coeffs)
                .take(1 << i)
                .collect();
            inv = self.mul_scalar_sub_poly(2, &inv, &prod);
        }

        Ok(inv)
    }

    #[inline(always)]
    fn div_rem_poly_approx<const N: usize>(
        &self,
        f: &Poly,
        g: &Poly,
        inv: &Poly,
    ) -> Result<[Poly; N], CompositeError> {
        assert!(N == 1 || N == 2);
        assert!(!g.is_zero());
        let mut out = array::from_fn(|_| Poly::zero());

        if g.deg() == 0 {
            if N == 2 {
                out[1] = self.div_scalar_poly(&g.coeffs[0], f)?;
            }
            return Ok(out);
        }

        if f.deg() < g.deg() {
            out[0] = f.clone();
            return Ok(out);
        }

        let m = f.deg() - g.deg();
        let iter = self
            .mul_poly_iter(f.coeffs.iter().rev().take(m + 1), &inv.coeffs)
            .take(m + 1)
            .rev();
        let len = iter.len();
        let q = (0..m - (len - 1))
            .map(|_| Integer::ZERO)
            .chain(iter)
            .collect();
        let r = self.sub_poly(f, &self.mul_poly(g, &q));

        out[0] = r;
        if N == 2 {
            out[1] = q;
        }

        Ok(out)
    }

    fn rem_poly_approx(&self, f: &Poly, g: &Poly, inv: &Poly) -> Result<Poly, CompositeError> {
        let [r] = self.div_rem_poly_approx(f, g, inv)?;
        Ok(r)
    }

    #[inline(always)]
    fn div_rem_poly<const N: usize>(
        &self,
        f: &Poly,
        g: &Poly,
    ) -> Result<[Poly; N], CompositeError> {
        assert!(N == 1 || N == 2);
        let buf;
        let (c, g) = if g.is_monic() {
            (None, g)
        } else {
            let (c, p) = self.make_monic(g)?;
            buf = p;
            (Some(c), &buf)
        };

        let inv = self.approx_poly_div(g, f.deg())?;
        let mut out = self.div_rem_poly_approx(f, g, &inv)?;

        if N == 2 {
            if let Some(c) = c {
                let q = mem::take(&mut out[1]);
                out[1] = self.mul_scalar_poly(&c, &q);
            }
        }

        Ok(out)
    }

    fn rem_poly_naive(&self, f: &Poly, g: &Poly) -> Result<Poly, CompositeError> {
        assert!(!g.is_zero());
        if g.deg() == 0 {
            return Ok(Poly::zero());
        }

        let mut f = f.clone();
        if f.deg() < g.deg() {
            return Ok(f);
        }

        let div = self.inv(g.leading())?;
        while f.deg() >= g.deg() {
            let pow = f.deg() - g.deg();
            let coeff_quot = self.mul(f.leading(), &div);
            f = self.sub_poly(&f, &(self.mul_scalar_poly(&coeff_quot, g) << pow));
        }

        Ok(f)
    }

    #[inline(always)]
    fn euclid_poly<const N: usize>(&self, f: &Poly, g: &Poly) -> Result<[Poly; N], CompositeError> {
        assert!(N == 1 || N == 2);
        let mut r0 = f.clone();
        let mut r1 = g.clone();
        let mut s0 = Poly::one();
        let mut s1 = Poly::zero();

        while !r1.is_zero() {
            let mut res = self.div_rem_poly::<N>(&r0, &r1)?;
            let (q, r2) = if N == 2 {
                (mem::take(&mut res[1]), mem::take(&mut res[0]))
            } else {
                (Poly::zero(), mem::take(&mut res[0])) // quotient is ignored
            };
            (r0, r1) = (r1, r2);

            if N == 2 {
                let s2 = self.sub_poly(&s0, &self.mul_poly(&q, &s1));
                (s0, s1) = (s1, s2);
            }
        }

        if !r0.is_monic() {
            let i;
            (i, r0) = self.make_monic(&r0)?;
            if N == 2 {
                s0 = self.mul_scalar_poly(&i, &s0);
            }
        }

        let mut out = array::from_fn(|_| Poly::zero());
        out[0] = r0;
        if N == 2 {
            out[1] = s0;
        }

        Ok(out)
    }

    pub(crate) fn gcd_poly(&self, f: &Poly, g: &Poly) -> Result<Poly, CompositeError> {
        let [d] = self.euclid_poly(f, g)?;
        Ok(d)
    }
}

pub(crate) struct CompositeError; // the suspected prime is actually composite

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct Poly { // a polynomial over Z/pZ
    coeffs: Vec<Integer>,
}

impl Poly {
    pub(crate) fn new(coeffs: Vec<Integer>) -> Self {
        let mut out = Self { coeffs };
        out.normalize();
        out
    }

    pub(crate) fn zero() -> Self {
        Self { coeffs: vec![] }
    }

    pub(crate) fn is_zero(&self) -> bool {
        self.coeffs.is_empty()
    }

    pub(crate) fn constant(a: Integer) -> Self {
        Self::new(vec![a])
    }

    pub(crate) fn one() -> Self {
        Self::constant(1.into())
    }

    pub(crate) fn x() -> Self {
        Self::new(vec![0.into(), 1.into()])
    }

    #[inline(always)]
    pub(crate) fn by_coeffs<'a, F, const N: usize>(polys: [&'a Poly; N], mut f: F) -> Self
    where
        F: FnMut([&'a Integer; N]) -> Integer,
    {
        const ZERO: &Integer = &Integer::ZERO;
        let min = polys.iter().map(|p| p.coeffs.len()).min().unwrap();
        let max = polys.iter().map(|p| p.coeffs.len()).max().unwrap();
        let mut coeffs = Vec::with_capacity(max);
        coeffs.extend((0..min).map(|i| f(array::from_fn(|j| &polys[j].coeffs[i]))));
        coeffs.extend((min..max).map(|i| {
            f(array::from_fn(|j| {
                let v = &polys[j].coeffs;
                if i < v.len() {
                    &v[i]
                } else {
                    ZERO
                }
            }))
        }));
        Self::new(coeffs)
    }

    fn is_monic(&self) -> bool {
        self.is_zero() || *self.leading() == 1
    }

    fn constant_part(&self) -> Self {
        if self.is_zero() {
            Self::zero()
        } else {
            Self::new(vec![self.coeffs[0].clone()])
        }
    }

    fn normalize(&mut self) {
        if !self.coeffs.is_empty() && *self.leading() == 0 {
            let cut = self.coeffs.iter().rev().take_while(|n| **n == 0).count();
            self.coeffs.truncate(self.coeffs.len() - cut);
        }
    }

    pub(crate) fn deg(&self) -> usize {
        self.coeffs.len().saturating_sub(1)
    }

    fn leading(&self) -> &Integer {
        &self.coeffs[self.deg()]
    }
}

impl Default for Poly {
    fn default() -> Self {
        Self::zero()
    }
}

impl FromIterator<Integer> for Poly {
    fn from_iter<T: IntoIterator<Item = Integer>>(iter: T) -> Self {
        Self::new(iter.into_iter().collect())
    }
}

impl Shl<usize> for Poly {
    type Output = Self;

    fn shl(mut self, rhs: usize) -> Self::Output {
        #[allow(clippy::suspicious_arithmetic_impl)]
        self.coeffs
            .resize_with(self.coeffs.len() + rhs, || Integer::ZERO);
        self.coeffs.rotate_right(rhs);
        self
    }
}

pub(crate) struct PolyQuotient { // a quotient ring (Z/pZ)[x]/(h)
    field: Field,
    div: Poly,
    inv: Vec<Poly>,
}

impl PolyQuotient {
    pub(crate) fn new(field: &Field, div: Poly) -> Result<Self, CompositeError> {
        assert!(!div.is_zero());
        assert!(div.is_monic());

        let field = field.clone();
        let mut inv = vec![Poly::one()];

        let m = div.deg();
        let k = (usize::BITS - m.leading_zeros()) as usize;
        for i in 1..=k {
            let old = &inv[i - 1];
            let prod = field
                .mul_poly_iter(div.coeffs.iter().rev(), &field.mul_poly(old, old).coeffs)
                .take(1 << i)
                .collect();
            inv.push(field.mul_scalar_sub_poly(2, old, &prod));
        }

        Ok(Self { field, div, inv })
    }

    fn approx(&self, f: &Poly) -> usize {
        let m = f.deg().saturating_sub(self.div.deg());
        (usize::BITS - m.leading_zeros()) as usize
    }

    pub(crate) fn reduce(&self, f: &Poly) -> Result<Poly, CompositeError> {
        let k = self.approx(f);
        if k >= self.inv.len() {
            self.field.rem_poly_naive(f, &self.div)
        } else {
            self.field.rem_poly_approx(f, &self.div, &self.inv[k])
        }
    }

    pub(crate) fn mul(&self, f: &Poly, g: &Poly) -> Result<Poly, CompositeError> {
        self.reduce(&self.field.mul_poly(f, g))
    }

    pub(crate) fn div(&self, f: &Poly, g: &Poly) -> Result<Poly, PolyQuotientError> {
        let inv = self.inv(g)?;
        Ok(self.mul(f, &inv)?)
    }

    fn inv(&self, f: &Poly) -> Result<Poly, PolyQuotientError> {
        let [d, i] = self.field.euclid_poly(f, &self.div)?;
        if d == Poly::one() {
            Ok(i)
        } else {
            Err(PolyQuotientError::PolyFactor(d))
        }
    }

    pub(crate) fn pow(&self, f: &Poly, n: &Integer) -> Result<Poly, CompositeError> {
        let mut out = Poly::one();
        let mut factor = f.clone();
        for i in 0..n.significant_bits() {
            if n.get_bit(i) {
                out = self.mul(&out, &factor)?;
            }
            factor = self.mul(&factor, &factor)?;
        }
        Ok(out)
    }

    pub(crate) fn compose_pair(
        &self,
        f1: &Poly,
        f2: &Poly,
        g: &Poly,
    ) -> Result<(Poly, Poly), CompositeError> {
        let field = &self.field;
        let mut out1 = f1.constant_part();
        let mut out2 = f2.constant_part();
        let mut pow = Poly::one();
        let min = f1.coeffs.len().min(f2.coeffs.len());

        for (a, b) in f1.coeffs[1..].iter().zip(&f2.coeffs[1..]) {
            pow = self.mul(&pow, g)?;
            out1 = field.mul_scalar_add_poly(a, &pow, &out1);
            out2 = field.mul_scalar_add_poly(b, &pow, &out2);
        }

        let (f, out) = match f1.deg().cmp(&f2.deg()) {
            Ordering::Less => (&f2, &mut out2),
            Ordering::Equal => return Ok((out1, out2)),
            Ordering::Greater => (&f1, &mut out1),
        };

        for a in &f.coeffs[min..] {
            pow = self.mul(&pow, g)?;
            *out = field.mul_scalar_add_poly(a, &pow, out);
        }

        Ok((out1, out2))
    }
}

pub(crate) enum PolyQuotientError {
    PolyFactor(Poly), // the quotient ring is not a field
    CompositeModulus, // the underlying ring is not a field
}

impl From<CompositeError> for PolyQuotientError {
    fn from(_: CompositeError) -> Self {
        Self::CompositeModulus
    }
}
