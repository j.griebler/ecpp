use std::iter;

use rayon::prelude::*;
use rug::{Complete, Integer};

use crate::{
    arith::{crt_update, CompositeError, Field, Poly, PolyQuotient, PolyQuotientError},
    primes::Sieve,
};

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct Point(Option<(Integer, Integer)>); // a rational point on an elliptic curve

impl Point {
    pub(crate) fn zero() -> Self {
        Self(None)
    }

    pub(crate) fn is_zero(&self) -> bool {
        self.0.is_none()
    }

    pub(crate) fn affine(x: Integer, y: Integer) -> Self {
        Self(Some((x, y)))
    }

    pub(crate) fn get(self) -> Option<(Integer, Integer)> {
        self.0
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct Curve { // an elliptic curve over a (presumed) finite field
    field: Field,
    a: Integer,
    b: Integer,
}

impl Curve {
    pub(crate) fn new(field: Field, a: Integer, b: Integer) -> Self {
        Self { field, a, b }
    }

    pub(crate) fn into_inner(self) -> (Integer, Integer, Integer) {
        let Self { field, a, b } = self;
        (field.into_inner(), a, b)
    }

    pub(crate) fn modulus(&self) -> &Integer {
        self.field.modulus()
    }

    pub(crate) fn add(&self, p: &Point, q: &Point) -> Result<Point, CompositeError> {
        let Self { field, a, .. } = self;
        match (&p.0, &q.0) {
            (None, _) => Ok(q.clone()), // 0 + Q = Q
            (_, None) => Ok(p.clone()), // P + 0 = P
            (Some((x1, y1)), Some((x2, y2))) => {
                let with_m = |m: Integer| {
                    let x3 = field.sub(field.sub(field.mul(&m, &m), x1), x2);
                    let y3 = field.sub(field.mul(m, field.sub(x1, &x3)), y1);
                    Ok(Point::affine(x3, y3))
                };

                if x1 != x2 { // generic addition
                    with_m(field.div(field.sub(y2, y1), &field.sub(x2, x1))?)
                } else if y1.is_zero() || y1 != y2 { // points are negatives
                    Ok(Point::zero())
                } else { // doubling
                    let num = field.add(field.mul(field.add(field.add(x1, x1), x1), x1), a);
                    let denom = field.add(y1, y1);
                    with_m(field.div(num, &denom)?)
                }
            }
        }
    }

    pub(crate) fn mul_scalar(&self, n: &Integer, p: &Point) -> Result<Point, CompositeError> {
        if *n == 0 {
            return Ok(Point::zero());
        }

        let mut out = p.clone();

        for i in (0..n.significant_bits() - 1).rev() { // double-and-add
            out = self.add(&out, &out)?;
            if n.get_bit(i) {
                out = self.add(&out, p)?;
            }
        }
        Ok(out)
    }

    fn poly(&self) -> Poly {
        Poly::new(vec![self.b.clone(), self.a.clone(), 0.into(), 1.into()])
    }

    fn division_polys(&self) -> DivisionPolys {
        DivisionPolys::new(self)
    }

    pub(crate) fn add_endo(
        &self,
        f: &Endo,
        g: &Endo,
        quot: &PolyQuotient,
    ) -> Result<Endo, PolyQuotientError> {
        let Curve { field, a, .. } = self;

        match (&f.0, &g.0) {
            (None, None) => Ok(Endo::zero()),
            (None, Some(_)) => Ok(g.clone()),
            (Some(_), None) => Ok(f.clone()),
            (Some(f), Some(g)) => {
                let poly = self.poly();
                let (r_num, r_denom) = if f.0 != g.0 { // generic addition
                    (field.sub_poly(&f.1, &g.1), field.sub_poly(&f.0, &g.0))
                } else if f.1 == g.1 { // doubling (we can't have f.1 == g.1 == 0 here)
                    (
                        field.mul_scalar_add_poly(
                            3,
                            &quot.mul(&f.0, &f.0)?,
                            &Poly::constant(a.clone()),
                        ),
                        field.mul_scalar_poly(2, &quot.mul(&f.1, &poly)?),
                    )
                } else { // negatives
                    return Ok(Endo::zero());
                };

                let r = quot.div(&r_num, &r_denom)?; // this might reveal a smaller factor
                let poly_x = Poly::by_coeffs(
                    [&quot.mul(&quot.mul(&r, &r)?, &poly)?, &f.0, &g.0],
                    |[c, d, e]| field.sub(c, field.add(d, e)),
                );
                let poly_y = field.sub_poly(&quot.mul(&r, &field.sub_poly(&f.0, &poly_x))?, &f.1);

                Ok(Endo::new(poly_x, poly_y))
            }
        }
    }

    pub(crate) fn mul_scalar_endo(
        &self,
        n: &Integer,
        f: &Endo,
        quot: &PolyQuotient,
    ) -> Result<Endo, PolyQuotientError> {
        if *n == 0 {
            return Ok(Endo::zero());
        }

        let mut out = f.clone();

        for i in (0..n.significant_bits() - 1).rev() { // double-and-add
            out = self.add_endo(&out, &out, quot)?;
            if n.get_bit(i) {
                out = self.add_endo(&out, f, quot)?;
            }
        }
        Ok(out)
    }

    pub(crate) fn mul_endo(
        &self,
        f: &Endo,
        g: &Endo,
        quot: &PolyQuotient,
    ) -> Result<Endo, CompositeError> {
        match (&f.0, &g.0) {
            (None, _) => Ok(Endo::zero()),
            (_, None) => Ok(Endo::zero()),
            (Some(f), Some(g)) => {
                let (c1, c2) = quot.compose_pair(&f.0, &f.1, &g.0)?;
                Ok(Endo::new(c1, quot.mul(&c2, &g.1)?))
            }
        }
    }

    pub(crate) fn count(&self) -> Result<Integer, CompositeError> {
        let q = self.modulus();

        let mut divs = self.division_polys();

        let mut m = Integer::from(1);
        let mut primes = vec![];
        for l in Sieve::new() {
            if (q % l).complete() == 0 {
                return Err(CompositeError);
            }

            primes.push(l);
            divs.compute(l as usize); // prepare division polynomials
            m *= l;
            if m.square_ref().complete() > (q << 4u32).complete() {
                break; // m > 4sqrt(q)
            }
        }

        let remainders = primes
            .par_iter()
            .map(|&l| self.trace_mod(&divs, l)) // compute all t_l in parallel
            .collect::<Result<Vec<_>, _>>()?;
        let mut t = iter::zip(remainders, primes)
            .fold((Integer::ZERO, Integer::from(1)), |(t, m), (tl, l)| {
                crt_update(t, m, tl, l)
            })
            .0;
        if t > (&m >> 1u32).complete() {
            t -= m; // outside of range, t must be negative
        }

        Ok(q - t + 1)
    }

    fn trace_mod(&self, divs: &DivisionPolys, l: u64) -> Result<u64, CompositeError> {
        let field = &self.field;
        if l == 2 {
            let poly = self.poly();
            let q = field.modulus();
            let x = Poly::x();
            let quot = PolyQuotient::new(field, poly.clone())?;
            let xq = quot.pow(&x, q)?;
            Ok(u64::from(
                field.gcd_poly(&poly, &field.sub_poly(&xq, &x))?.deg() == 0,
            ))
        } else {
            let mut h = divs.get_monic(l as usize)?;
            loop {
                let result = self.trace_mod_with_div(h, l);
                if let Err(PolyQuotientError::PolyFactor(d)) = result {
                    h = d; // retry with smaller factor
                } else {
                    return result.map_err(|_| CompositeError);
                }
            }
        }
    }

    fn trace_mod_with_div(&self, h: Poly, l: u64) -> Result<u64, PolyQuotientError> {
        let field = &self.field;
        let q = field.modulus();
        let quot = PolyQuotient::new(field, h)?;
        let (xq, yq) = rayon::join( // compute xq, yq in parallel
            || quot.pow(&Poly::x(), q),
            || quot.pow(&self.poly(), &(q >> 1u32).complete()),
        );
        let (xq, yq) = (xq?, yq?);
        let pi = Endo::new(xq, yq);
        let id = Endo::id(&quot)?;
        let (pi2, ql) = rayon::join( // compute pi^2, p in parallel
            || self.mul_endo(&pi, &pi, &quot),
            || self.mul_scalar_endo(&(q % l).complete(), &id, &quot),
        );
        let (pi2, ql) = (pi2?, ql?);
        let pi2_ql = self.add_endo(&pi2, &ql, &quot)?; // pi^2 + p
        let Some((poly_x, poly_y)) = pi2_ql.0 else {
            return Ok(0);
        };

        let mut sum = Endo::zero();
        for i in 1..(l + 1) / 2 {
            sum = self.add_endo(&sum, &pi, &quot)?;
            let (sum_x, sum_y) = sum.0.as_ref().unwrap();
            if *sum_x == poly_x { // +/- i*pi = pi^2 + p
                return if *sum_y == poly_y { Ok(i) } else { Ok(l - i) };
            }
        }

        Err(PolyQuotientError::CompositeModulus)
    }
}

struct DivisionPolys {
    field: Field,
    polys: Vec<Poly>,
    curve_poly2_16: Poly,
}

impl DivisionPolys {
    fn new(curve: &Curve) -> Self {
        let Curve { field, a, b } = curve;
        let field = field.clone();
        let a2 = field.mul(a, a);
        let psi1 = Poly::new(vec![1.into()]);
        let psi2 = Poly::new(vec![1.into()]);
        let psi3 = Poly::new(vec![
            field.neg(&a2),
            field.mul(12, b),
            field.mul(6, a),
            0.into(),
            3.into(),
        ]);
        let psi4 = Poly::new(vec![
            field.sub(
                field.mul(field.neg(2), field.mul(a, &a2)),
                field.mul(16, field.mul(b, b)),
            ),
            field.mul(field.neg(8), field.mul(a, b)),
            field.mul(field.neg(10), a2),
            field.mul(40, b),
            field.mul(10, a),
            0.into(),
            2.into(),
        ]);
        let polys = vec![psi1, psi2, psi3, psi4];
        let curve_poly = curve.poly();
        let curve_poly2_16 = field.mul_scalar_poly(16, &field.mul_poly(&curve_poly, &curve_poly));
        Self {
            field,
            polys,
            curve_poly2_16,
        }
    }

    fn get_monic(&self, n: usize) -> Result<Poly, CompositeError> {
        self.field.make_monic(&self.polys[n - 1]).map(|p| p.1)
    }

    fn compute(&mut self, n: usize) {
        let i = n - 1;
        if self.polys.len() < n {
            self.polys.resize(n, Poly::zero());
        }
        if self.polys[i].is_zero() {
            let k = n / 2;
            self.compute(k - 1);
            self.compute(k);
            self.compute(k + 1);
            self.compute(k + 2);
            if n % 2 != 0 {
                let field = &self.field;
                let psi_km1 = &self.polys[k - 2]; // indices are shifted by 1
                let psi_k = &self.polys[k - 1];
                let psi_kp1 = &self.polys[k];
                let psi_kp2 = &self.polys[k + 1];
                let l = field.mul_poly(
                    &field.mul_poly(psi_kp2, psi_k),
                    &field.mul_poly(psi_k, psi_k),
                );
                let r = field.mul_poly(
                    &field.mul_poly(psi_km1, psi_kp1),
                    &field.mul_poly(psi_kp1, psi_kp1),
                );
                let (l, r) = if k % 2 == 0 { // account for the implicit factor of 16y^4
                    (field.mul_poly(&l, &self.curve_poly2_16), r)
                } else {
                    (l, field.mul_poly(&r, &self.curve_poly2_16))
                };
                self.polys[i] = field.sub_poly(&l, &r);
            } else {
                self.compute(k - 2);
                let field = &self.field;
                let psi_km2 = &self.polys[k - 3]; // indices are shifted by 1
                let psi_km1 = &self.polys[k - 2];
                let psi_k = &self.polys[k - 1];
                let psi_kp1 = &self.polys[k];
                let psi_kp2 = &self.polys[k + 1];
                let l = field.mul_poly(psi_kp2, &field.mul_poly(psi_km1, psi_km1));
                let r = field.mul_poly(psi_km2, &field.mul_poly(psi_kp1, psi_kp1));
                self.polys[i] = field.mul_poly(psi_k, &field.sub_poly(&l, &r));
            }
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub(crate) struct Endo(Option<(Poly, Poly)>); // an element of End(E[l])

impl Endo {
    pub(crate) fn new(poly_x: Poly, poly_y: Poly) -> Self {
        Self(Some((poly_x, poly_y)))
    }

    pub(crate) fn id(quot: &PolyQuotient) -> Result<Self, CompositeError> {
        Ok(Self::new(
            quot.reduce(&Poly::new(vec![0.into(), 1.into()]))?,
            Poly::one(),
        ))
    }

    pub(crate) fn zero() -> Self {
        Self(None)
    }
}
