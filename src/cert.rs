use std::{fmt, str::FromStr};

use rug::{ops::Pow, Complete, Integer};

use crate::{
    arith::Field,
    curve::{Curve, Point},
    primes::{Sieve, SMALL_PRIMES},
};

pub(crate) struct PrimalityCert {
    pub(crate) p: Integer,
    pub(crate) a: Integer,
    pub(crate) b: Integer,
    pub(crate) x: Integer,
    pub(crate) y: Integer,
    pub(crate) q: Integer,
}

impl PrimalityCert {
    pub(crate) fn check(self, prev: Integer) -> Option<Integer> {
        let Self { p, a, b, x, y, q } = self;
        if p <= 1
            || q != prev                   // q not verified
            || disc_gcd(&a, &b, &p) != 1   // bad reduction
            || !on_curve(&a, &b, &x, &y)   // not on curve
            || !is_bounded(&q, &p)         // q too small
        {
            return None;
        }

        let field = Field::new(p);
        let curve = Curve::new(field, a, b);
        let point = Point::affine(x, y);
        let mul = curve.mul_scalar(&q, &point).ok()?;
        mul.is_zero().then_some(curve.into_inner().0)
    }
}

impl fmt::Display for PrimalityCert {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Self { p, a, b, x, y, q } = self;
        write!(f, "({p}, {a}, {b}, {x}, {y}, {q})")
    }
}

impl FromStr for PrimalityCert {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() < 2 || &s[..1] != "(" || &s[s.len() - 1..] != ")" {
            return Err(());
        }
        let mut pieces = s[1..s.len() - 1].split(", ");
        let p = pieces.next().ok_or(())?.parse().map_err(|_| ())?;
        let a = pieces.next().ok_or(())?.parse().map_err(|_| ())?;
        let b = pieces.next().ok_or(())?.parse().map_err(|_| ())?;
        let x = pieces.next().ok_or(())?.parse().map_err(|_| ())?;
        let y = pieces.next().ok_or(())?.parse().map_err(|_| ())?;
        let q = pieces.next().ok_or(())?.parse().map_err(|_| ())?;
        Ok(Self { p, a, b, x, y, q })
    }
}

pub struct PrimalityProof {
    start: u16,
    certs: Vec<PrimalityCert>,
}

impl PrimalityProof {
    pub(crate) fn new(start: u16) -> Self {
        Self {
            start,
            certs: vec![],
        }
    }

    pub(crate) fn push(&mut self, cert: PrimalityCert) {
        self.certs.push(cert);
    }

    pub fn check(self) -> Option<Integer> {
        let Self { start, certs } = self;
        if !is_small_prime(start) {
            return None;
        }

        let mut prime = start.into();
        for cert in certs {
            prime = cert.check(prime)?;
        }

        Some(prime)
    }
}

impl fmt::Display for PrimalityProof {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(cert) = self.certs.last() {
            writeln!(f, "{} is prime", cert.p)?;
        } else {
            return writeln!(f, "{} is prime", self.start);
        }

        for cert in &self.certs {
            writeln!(f, "{cert}")?;
        }
        Ok(())
    }
}

impl FromStr for PrimalityProof {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.lines();
        let first = lines.next().ok_or(())?;
        let certs = lines
            .map(|l| l.parse())
            .collect::<Result<Vec<PrimalityCert>, _>>()
            .map_err(|_| ())?;

        let ix = first.find(' ').ok_or(())?;
        if &first[ix..] != " is prime" {
            return Err(());
        }
        let start = if certs.is_empty() {
            first[..ix].parse().map_err(|_| ())?
        } else {
            if first[..ix].parse::<Integer>().map_err(|_| ())? != certs[certs.len() - 1].p {
                return Err(());
            }
            u16::try_from(&certs[0].q).map_err(|_| ())?
        };

        Ok(Self { start, certs })
    }
}

pub(crate) fn is_small_prime(n: u16) -> bool {
    if n < 256 {
        (SMALL_PRIMES[usize::from(n) / 64] >> (n % 64)) & 1 != 0
    } else {
        Sieve::new().take(54).all(|k| n % (k as u16) != 0)
    }
}

pub(crate) fn is_bounded(q: &Integer, p: &Integer) -> bool {
    let root = p.root_ref(4).complete() + 1u32;
    *q > root.square()
}

pub(crate) fn disc_gcd(a: &Integer, b: &Integer, p: &Integer) -> Integer {
    (4u32 * a.clone().pow(3) + 27u32 * b.clone().square()).gcd(p)
}

fn on_curve(a: &Integer, b: &Integer, x: &Integer, y: &Integer) -> bool {
    y.square_ref().complete() == (x.square_ref() + a).complete() * x + b
}
