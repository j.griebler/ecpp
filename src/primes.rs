pub(crate) const SMALL_PRIMES: [u64; 4] = {
    let mut primes = [!0; 4];
    primes[0] &= !3;
    let mut d = 2;
    while d * d < 256 {
        let (k, l) = indices(d);
        if (primes[k] >> l) & 1 != 0 {
            let mut m = d + d;
            while m < 256 {
                let (k, l) = indices(m);
                primes[k] &= !(1 << l);
                m += d;
            }
        }
        d += 1;
    }
    primes // bitset of primes below 256
};

const fn indices(i: u64) -> (usize, u32) {
    (i as usize / 64, (i % 64) as u32)
}

pub(crate) struct Sieve {
    sieve: Vec<u64>,
    next: u64,
}

impl Sieve {
    pub(crate) fn new() -> Self {
        Self {
            sieve: SMALL_PRIMES.to_vec(),
            next: 2,
        }
    }

    pub(crate) fn len(&self) -> u64 {
        self.sieve.len() as u64 * 64
    }

    pub(crate) fn get(&self, i: u64) -> bool {
        let (k, l) = indices(i);
        (self.sieve[k] >> l) & 1 != 0
    }

    fn clear(&mut self, i: u64) {
        let (k, l) = indices(i);
        self.sieve[k] &= !(1 << l);
    }

    pub(crate) fn grow(&mut self) {
        let old_len = self.len();
        self.sieve.extend_from_slice(&[!0; 4]);
        let mut k = 2;
        while k * k < self.len() {
            if self.get(k) {
                let mut m = (old_len + k - 1) / k * k;
                while m < self.len() {
                    self.clear(m);
                    m += k;
                }
            }
            k += 1;
        }
    }
}

impl Iterator for Sieve {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        let (mut chunk, mut pos) = indices(self.next);
        loop {
            if chunk >= self.sieve.len() {
                self.grow();
            }
            let bits = self.sieve[chunk];
            let new_pos = u64::from((bits & !((1 << pos) - 1)).trailing_zeros());
            if new_pos < 64 {
                self.next = chunk as u64 * 64 + new_pos + 1;
                return Some(self.next - 1);
            }
            chunk += 1;
            pos = 0;
        }
    }
}
