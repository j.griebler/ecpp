use std::{env, fs, io};

use rug::Integer;

use ecpp::*;

fn main() {
    match run() {
        Err(Error::UsageError) => eprintln!("usage:\n  ecpp certify <N>\n  ecpp check <N>"),
        Err(Error::IoError(e)) => eprintln!("error: {e}"),
        _ => (),
    }
}

fn run() -> Result<(), Error> {
    let mut args = env::args();
    let cmd = args.nth(1).ok_or(Error::UsageError)?;

    match &cmd[..] {
        "certify" => {
            let n = args
                .next()
                .ok_or(Error::UsageError)?
                .parse::<Integer>()
                .map_err(|_| Error::UsageError)?;
            if let Some(proof) = ecpp(n) {
                print!("{proof}");
            } else {
                println!("not prime");
            }
        }
        "check" => {
            let input = if let Some(path) = args.next() {
                fs::read_to_string(path)?
            } else {
                io::read_to_string(io::stdin())?
            };

            if let Ok(proof) = input.parse::<PrimalityProof>() {
                if let Some(p) = proof.check() {
                    println!("proof is valid, {p} is prime");
                } else {
                    println!("could not validate proof");
                }
            } else {
                println!("could not parse proof");
            }
        }
        _ => return Err(Error::UsageError),
    }

    Ok(())
}

enum Error {
    UsageError,
    IoError(io::Error),
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self::IoError(e)
    }
}
